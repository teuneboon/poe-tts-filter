import sys

from poe_tts_filter.formatter import format_linked_fast
from poe_tts_filter.generator.classes import get_classes
from poe_tts_filter.generator.currency import get_currency
from poe_tts_filter.generator.links import get_all_link_colors
from poe_tts_filter.generator.rarities import get_all_rarities


def generate_list():
    if input('Are you sure you want to generate? This will overwrite the current list.txt [y/N] ').lower() != 'y':
        sys.exit(1)

    lines = []
    lines += get_currency(short=True)
    for color in get_all_link_colors():
        for rarity in get_all_rarities():
            # creates a line like "GGBB rare gloves"
            lines += [format_linked_fast(color, rarity, base_class) for base_class in get_classes(linkable=1, short=True)]

    for color in get_all_link_colors(links=3):
        for rarity in get_all_rarities():
            for class_ in ['Wand', 'Sceptre']:
                lines.append(format_linked_fast(color, rarity, class_))

    lines += get_classes(short=True, linkable=0)

    lines += ['Rare Wand', 'Rare Two Stone', 'Rare Sapphire', 'Rare Topaz', 'Rare Ruby', 'Rare Coral', 'Rare Jade', 'Rare Leather Belt', 'Rare Heavy Belt', 'Rare Ring', 'Rare Belt', 'Rare Amulet']

    with open('list.txt', 'w') as list_file:
        list_file.writelines(map('{0}\n'.format, lines))


if __name__ == '__main__':
    generate_list()
