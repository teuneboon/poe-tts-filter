# poe-tts-filter

Based on Fightgarr's run: https://www.youtube.com/watch?v=jojMV60ntBE I decided to make a tool to help make TTS(text-to-speech) filters in Path of Exile.

## If you just want to download
Download from http://filter.1337.gd/speech.zip and unpack it in the place you normally place your filters, tts.filter includes instructions(search for "REPLACE:" in there) on how to edit your filter to make it fit. Was intended to be used with StupidFatHobbit's filter but might work in others too.


## Features
- Create mp3 files from a set of predefined "texts"
- Create a set of predefined texts for PoE related stuff, like currency, 4L colours etc. (still very much Work In Progress)
- Create a .filter file that you can use to replace parts of StupidFatHobbit's filter(or your own, style is based on Hobbit's though)

## How to use
1. Install Python3
2. Open terminal
3. `pip3 install -r requirements.txt`
4. You can now edit list.txt in any editor, one line per "text" to TTS
5. `python run.py` this will convert all texts to mp3 files which are placed in `speech/`
6. `python generate.py` will generate a base list.txt file, Python knowledge required to edit this
7. `python filter.py` will generate a tts.filter + mp3 files. A little bit more Python knowledge required to edit this one again(currently very incomplete)

## Upcoming/wanted list(feel free to make a merge request!)
- Automatically create a base filter based on this
- Allow filter file as an input, take all `CustomAlertSound <sound here>` appearances , and if the "sound" starts with `tts-`(for example: `tts-chromatic-orb.mp3`) automatically create a .mp3 based on that.
- Make generating the list require less Python knowledge
- Suggestions? Send an issue   


## Credits
- I used some of Neversink's filter rules to get started, including styling
- I based some stuff like the 4L system on StupidFatHobbit's filter
