import os

from poe_tts_filter.tts_providers.google import GoogleTTSProvider


def generate_filename(text):
    return '{0}.mp3'.format(text.strip().lower().replace(' ', '-'))


def generate_files():
    tts_provider = GoogleTTSProvider()
    with open('list.txt', 'r') as list_file:
        lines = list_file.readlines()

    print('Generating speech for {0} lines, this might take a while...'.format(len(lines)))
    for line in lines:
        if line:
            path = 'speech/{0}'.format(generate_filename(line))
            if os.path.exists(path):
                print('Not generating {0} since it already exists'.format(path))
            else:
                print('Generating {0}'.format(path))
                tts_provider.create_file(line, path)


if __name__ == '__main__':
    generate_files()
