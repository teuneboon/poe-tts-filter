import os

from poe_tts_filter.formatter import format_linked_fast
from poe_tts_filter.generator.classes import get_classes
from poe_tts_filter.generator.currency import get_currency
from poe_tts_filter.generator.links import get_all_link_colors
from poe_tts_filter.generator.rarities import get_all_rarities
from poe_tts_filter.tts_providers.google import GoogleTTSProvider
from run import generate_filename


def generate_if_not_exists(tts_provider, text, path):
    if os.path.exists(path):
        print('Not generating {0} since it already exists'.format(path))
    else:
        print('Generating {0}'.format(path))
        tts_provider.create_file(text, path)


def create_filter():
    tts_provider = GoogleTTSProvider()

    lines = ['# Just search for "REPLACE" to see instructions on how to update your filter for sounds']
    lines.append('# There\'s also stuff like "rare-topaz.mp3", "rare-wand.mp3", "rare-ring.mp3", "rare-jade.mp3" and "rare-amulet.mp3", no instructions on how to set those up here yet, you\'ll have to do those manually for now')
    lines.append('# REPLACE: Search for the 4L section in your filter, and replace it with everything until the next REPLACE')
    generate_four_link_lines(lines, tts_provider)

    lines.append('# 4L SECTION STOPS HERE')
    lines.append('')

    lines.append('# REPLACE: Replace the Caster Links section(3L wands/sceptres) of your filter with this(for Hobbit\'s just search for "Caster Links")')
    for color in get_all_link_colors(links=3):
        # we generate some 3L wand lines
        for rarity in get_all_rarities():
            for class_ in ['Wand', 'Sceptre']:
                text = format_linked_fast(color, rarity, class_)
                path = 'speech/{0}'.format(generate_filename(text))

                generate_if_not_exists(tts_provider, text, path)

                lines.append('Show')
                lines.append('    Class "{0}"'.format(class_))
                lines.append('    ItemLevel <= 25')
                lines.append('    LinkedSockets 3')
                lines.append('    SocketGroup {0}'.format(color))
                lines.append('    SetBorderColor 150 200 250')
                lines.append('    SetBackgroundColor 40 50 60')
                lines.append('    SetFontSize 45')
                lines.append('    Rarity = {0}'.format(rarity))
                lines.append('    CustomAlertSound "{0}"'.format(path))

    lines.append('# CASTER LINKS SECTION STOPS HERE')
    lines.append('')

    lines.append('# REPLACE: the following section is the currency section, in StupidFatHobbit\'s Filter you replace the first Racing currency block(section 0) with this(the transmute/alt/blacksmith one')

    generate_currency_lines(lines, tts_provider)

    lines.append('# CURRENCY SECTION STOPS HERE')
    lines.append('')
    lines.append('# REPLACE: Early sapphire + topaz ring, in Hobbit\'s filter you can just search for "Section 13" and replace the first 2 blocks there')

    for class_ in get_classes(short=True, linkable=0):
        generate_if_not_exists(tts_provider, class_, 'speech/{0}'.format(generate_filename(class_)))

    lines.append('''#mervail
Show
    BaseType "Sapphire Ring"
    ItemLevel <= 14
    SetBorderColor 150 120 255
    SetBackgroundColor 50 40 80
    SetFontSize 45
    CustomAlertSound "speech/sapphire.mp3"
#fidelitas
Show
    BaseType "Topaz Ring"
    ItemLevel <= 16
    SetBorderColor 150 120 255
    SetBackgroundColor 50 40 80
    SetFontSize 45
    CustomAlertSound "speech/topaz.mp3"''')

    extra_sounds = ['Rare Wand', 'Rare Two Stone', 'Rare Sapphire', 'Rare Topaz', 'Rare Ruby', 'Rare Coral', 'Rare Jade', 'Rare Leather Belt', 'Rare Heavy Belt', 'Rare Ring', 'Rare Belt', 'Rare Amulet']
    for extra_sound in extra_sounds:
        generate_if_not_exists(tts_provider, extra_sound, 'speech/{0}'.format(generate_filename(extra_sound)))

    with open('tts.filter', 'w') as filter_file:
        filter_file.writelines(map('{0}\n'.format, lines))


def generate_currency_lines(lines, tts_provider):
    no_style = ['Orb of Transmutation', 'Orb of Augmentation', 'Armourer\'s Scrap', 'Blacksmith\'s Whetstone', 'Silver Coin']
    style_1 = ['Chromatic Orb', 'Orb of Alteration', 'Glassblower\'s Bauble', 'Jeweller\'s Orb', 'Orb of Chance',
               'Orb of Regret']
    # @TODO: maybe move this to currency.py?
    default_sounds = {
        'Mirror of Kalandra': {
            'sound': 'ShMirror',
            'value': 1,
        },
        'Exalted Orb': {
            'sound': 'ShExalted',
            'value': 1,
        },
        'Divine Orb': {
            'sound': 'ShDivine',
            'value': 1,
        },
        'Chaos Orb': {
            'sound': 'ShChaos',
            'value': 2,
        },
        'Regal Orb': {
            'sound': 'ShRegal',
            'value': 2,
        },
        'Gemcutter\'s Prism': {
            'sound': 2,
            'value': 2,
        },
        'Vaal Orb': {
            'sound': 'ShVaal',
            'value': 2,
        },
        'Orb of Fusing': {
            'sound': 'ShFusing',
            'value': 2,
        },
        'Cartographer\'s Chisel': {
            'sound': 2,
            'value': 2,
        },
        'Orb of Alchemy': {
            'sound': 'ShAlchemy',
            'value': 2,
        },
        'Orb of Scouring': {
            'sound': 2,
            'value': 2,
        },
        'Blessed Orb': {
            'sound': 2,
            'value': 2,
        },
        'Portal Scroll': {
            'sound': None,
            'value': 3,
        },
        'Scroll of Wisdom': {
            'sound': None,
            'value': 3,
        },
    }
    for currency in get_currency(full_output=True, non_shaper_only=False):
        path = 'speech/{0}'.format(generate_filename(currency['short']))

        generate_if_not_exists(tts_provider, currency['short'], path)

        if currency['full'] in no_style + style_1:
            lines.append('Show')
            lines.append('    BaseType "{0}"'.format(currency['full']))
            lines.append('    SetFontSize 45')
            lines.append('    CustomAlertSound "{0}"'.format(path))
            lines.append('    MinimapIcon 2 Green Circle')
            if currency['full'] in style_1:
                lines.append('    SetBackgroundColor 170 158 130')
                lines.append('    SetTextColor 0 0 0')
                lines.append('    SetBorderColor 0 0 0')

    for currency, info in default_sounds.items():
        lines.append('Show')
        lines.append('    BaseType "{0}"'.format(currency))
        if info['sound'] is not None:
            lines.append('    PlayAlertSound {0} 300'.format(info['sound']))
        if info['value'] == 1:
            lines.append('    SetFontSize 45')
            lines.append('    MinimapIcon 0 Red Star')
            lines.append('    PlayEffect Red')
            lines.append('    SetTextColor 255 0 0 255')
            lines.append('    SetBorderColor 255 0 0 255')
            lines.append('    SetBackgroundColor 255 255 255 255')
        elif info['value'] == 2:
            lines.append('    SetFontSize 45')
            lines.append('    MinimapIcon 2 White Circle')
            lines.append('    PlayEffect White')
            lines.append('    SetTextColor 0 0 0 255')
            lines.append('    SetBorderColor 0 0 0 255')
            lines.append('    SetBackgroundColor 249 150 25 255')
        else:
            lines.append('    SetFontSize 40')
            lines.append('    SetTextColor 170 158 130 220')
            lines.append('    SetBorderColor 100 50 30 255')
            lines.append('    SetBackgroundColor 0 0 0 255')


def generate_four_link_lines(lines, tts_provider):
    for color in get_all_link_colors():
        for rarity in get_all_rarities():
            for base_class in get_classes(linkable=1, full_output=True):
                text = format_linked_fast(color, rarity, base_class['short'])
                path = 'speech/{0}'.format(generate_filename(text))

                generate_if_not_exists(tts_provider, text, path)

                lines.append('Show')
                lines.append('    Class "{0}"'.format(base_class['full']))
                lines.append('    ItemLevel <= 67')
                lines.append('    LinkedSockets 4')
                lines.append('    Sockets < 6')
                lines.append('    SocketGroup {0}'.format(color))
                lines.append('    Rarity = {0}'.format(rarity))
                lines.append('    CustomAlertSound "{0}"'.format(path))
                lines.append('    SetFontSize 45')
                lines.append('    SetBorderColor 0 255 255')
                lines.append('    SetBackgroundColor 0 65 65')
                lines.append('    MinimapIcon 2 Green Square')


if __name__ == '__main__':
    create_filter()
