def get_all_link_color_counts(links=4) -> list:
    result = []
    # @TODO: feel like this could be much more efficient
    for reds in range(0, links + 1):
        for greens in range(0, links + 1 - reds):
            for blues in range(0, links + 1 - reds - greens):
                if reds + greens + blues == links:
                    result.append((reds, greens, blues))
    return result


def get_all_link_colors(short=True, links=4) -> list:
    if short:
        red_text = 'R'
        green_text = 'G'
        blue_text = 'B'
    else:
        red_text = 'Red '
        green_text = 'Green '
        blue_text = 'Blue '

    result = []
    for reds, greens, blues in get_all_link_color_counts(links=links):
        result.append('{0}{1}{2}'.format(red_text * reds, green_text * greens, blue_text * blues))

    return result
