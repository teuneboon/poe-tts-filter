# @TODO: add currencies that don't have shaper voiceline
CURRENCY = {
    'Scroll of Wisdom': {
        'has_shaper': False,
        'short': 'Wisdom',
    },
    'Portal Scroll': {
        'has_shaper': False,
        'short': 'Portal',
    },
    'Armourer\'s Scrap': {
        'has_shaper': False,
        'short': 'Armourer',
    },
    'Orb of Augmentation': {
        'has_shaper': False,
        'short': 'Aug'
    },
    'Orb of Transmutation': {
        'has_shaper': False,
        'short': 'Transmute',
    },
    'Blacksmith\'s Whetstone': {
        'has_shaper': False,
        'short': 'Blacksmith',
    },
    'Orb of Chance': {
        'has_shaper': False,
        'short': 'Chance',
    },
    'Orb of Alteration': {
        'has_shaper': False,
        'short': 'Alt',
    },
    'Chromatic Orb': {
        'has_shaper': False,
        'short': 'Chrome',
    },
    'Jeweller\'s Orb': {
        'has_shaper': False,
        'short': 'Jew',
    },
    'Glassblower\'s Bauble': {
        'has_shaper': False,
        'short': 'Glassblower',
    },
    'Silver Coin': {
        'has_shaper': False,
    },
    'Orb of Scouring': {
        'has_shaper': False,
        'short': 'Scour',
    },
    'Orb of Regret': {
        'has_shaper': False,
        'short': 'Regret',
    }
}


def get_currency(short=False, non_shaper_only=True, full_output=False) -> list:
    result = []
    for name, data in CURRENCY.items():
        if not non_shaper_only or not data['has_shaper']:
            if full_output:
                result.append({
                    'full': name,
                    'short': data.get('short', name),
                })
            elif short:
                result.append(data.get('short', name))
            else:
                result.append(name)
    return result
