# @TODO: make complete, a lot of shit is missing
CLASSES = {
    'Boots': {
        'linkable': True,
    },
    'Gloves': {
        'linkable': True,
    },
    'Body Armour': {
        'linkable': True,
        'short': 'Chest',
    },
    'Helmet': {
        'linkable': True,
        'short': 'Helm',
    },
    'Topaz Ring': {
        'short': 'Topaz',
    },
    'Sapphire Ring': {
        'short': 'Sapphire',
    },
    'Ruby Ring': {
        'short': 'Ruby',
    },
}


def get_classes(short=False, linkable=-1, full_output=False) -> list:
    result = []
    for name, data in CLASSES.items():
        if linkable == -1 or int(data.get('linkable', 0)) == linkable:
            if full_output:
                result.append({
                    'full': name,
                    'short': data.get('short', name),
                })
            elif short:
                result.append(data.get('short', name))
            else:
                result.append(name)

    return result
