def get_all_rarities() -> list:
    return ['Normal', 'Magic', 'Rare', 'Unique']
