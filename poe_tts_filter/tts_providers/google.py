from poe_tts_filter.tts_providers import BaseTTSProvider

from gtts import gTTS


class GoogleTTSProvider(BaseTTSProvider):
    def create_file(self, text, filename):
        return gTTS(text=text, slow=False, lang='en').save(filename)
