def format_linked_basic(color, rarity, base_class):
    return '{2} {0} {1}'.format(color, rarity, base_class)


def format_linked_fast(color, rarity, base_class):
    reds = color.count('R')
    blues = color.count('B')
    greens = color.count('G')

    colors = []
    if reds:
        colors.append('{0} R'.format(reds))
    if blues:
        colors.append('{0} B'.format(blues))
    if greens:
        colors.append('{0} G'.format(greens))
    return '{2} {0} {1}'.format(' '.join(colors), rarity, base_class)
