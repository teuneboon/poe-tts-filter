from filter import generate_four_link_lines, generate_if_not_exists, generate_currency_lines
from poe_tts_filter.generator.classes import get_classes
from poe_tts_filter.tts_providers.google import GoogleTTSProvider
from run import generate_filename


def highlighted_rare(base_type, sound=None):
    if sound is None:
        sound = 'speech/rare-{0}.mp3'.format(base_type.replace(' ', '-').lower())

    return '''Show
    BaseType "{0}"
    Rarity Rare
    SetFontSize 45
    SetBackgroundColor 0 75 30 255
    SetBorderColor 150 150 150
    SetTextColor 255 255 119 255
    CustomAlertSound "{1}"'''.format(base_type, sound)


def create_filter():
    tts_provider = GoogleTTSProvider()

    extra_sounds = ['Rare Two Stone', 'Rare Sapphire', 'Rare Topaz', 'Rare Ruby', 'Rare Coral', 'Rare Iron',
                    'Rare Jade', 'Rare Amber', 'Rare Lapis', 'Rare Leather Belt', 'Rare Heavy Belt', 'Rare Rustic Sash',
                    'Magic Boots', 'Rare Boots', '5 Link', 'Large Life Flask', 'Greater Life Flask', 'Large Mana Flask',
                    'Amber Amulet', 'Jade Amulet', 'Quicksilver Flask', 'Onslaught Flask', 'Granite Flask', 'Jade Flask',
                    'Stibnite Flask']
    for extra_sound in extra_sounds:
        generate_if_not_exists(tts_provider, extra_sound, 'speech/{0}'.format(generate_filename(extra_sound)))

    lines = []
    # 6 links
    lines.append('''Show
    LinkedSockets 6
    SetFontSize 45
    SetTextColor 255 0 0 255
    SetBorderColor 255 0 0 255
    SetBackgroundColor 255 255 255 255
    PlayAlertSound 6 300
    PlayEffect Red
    MinimapIcon 0 Red Star''')
    # 5 links
    lines.append('''Show
    LinkedSockets 5
    Class "Body Armours"
    SetFontSize 45
    SetBorderColor 0 240 190 255
    CustomAlertSound "speech/5-link.mp3"
    PlayEffect White
    MinimapIcon 2 White Circle''')
    # we want unique items to show above almost all other rules
    lines.append('''Show
    Rarity = Unique
    SetFontSize 45
    SetTextColor 175 96 37 255
    SetBorderColor 175 96 37 255
    SetBackgroundColor 53 13 13 255
    PlayAlertSound 3 300
    PlayEffect Brown
    MinimapIcon 2 Brown Star''')
    # 6 sockets
    lines.append('''Show
    Sockets 6
    SetFontSize 45
    SetTextColor 255 255 255 255
    SetBorderColor 255 255 255 255
    SetBackgroundColor 100 100 100 255
    PlayAlertSound 7 300
    PlayEffect White
    MinimapIcon 2 White Circle''')
    lines.append('''Show
    Class "Divination"
    SetFontSize 45
    SetTextColor 0 0 0 255
    SetBorderColor 0 100 150 255
    SetBackgroundColor 145 215 230 225
    PlayAlertSound 2 300
    PlayEffect White Temp
    MinimapIcon 2 White Triangle''')
    generate_four_link_lines(lines, tts_provider)
    lines.append('')
    generate_currency_lines(lines, tts_provider)
    lines.append('''Show
    Class Currency
    BaseType "Essence of"
    SetFontSize 45
    SetBackgroundColor 0 122 255 255
    SetTextColor 255 255 255 255
    SetBorderColor 255 255 255 255
    PlayAlertSound 2 300
    PlayEffect Yellow
    MinimapIcon 2 Yellow Circle''')

    for class_ in get_classes(short=True, linkable=0):
        generate_if_not_exists(tts_provider, class_, 'speech/{0}'.format(generate_filename(class_)))

    lines.append('''Show
    BaseType "Rustic Sash"
    Rarity < Rare
    ItemLevel <= 12
    SetFontSize 45
Show
    BaseType "Amber Amulet"
    ItemLevel <= 16
    SetFontSize 45
    SetBorderColor 100 100 100 255
    CustomAlertSound "speech/amber-amulet.mp3"
Show
    BaseType "Jade Amulet"
    ItemLevel <= 16
    SetFontSize 45
    SetBorderColor 100 100 100 255
    CustomAlertSound "speech/jade-amulet.mp3"
Show
    BaseType "Iron Ring"
    ItemLevel <= 16
    SetFontSize 45
    SetBorderColor 100 100 100 255
#merveil
Show
    BaseType "Sapphire Ring"
    ItemLevel <= 14
    SetBorderColor 150 120 255
    SetBackgroundColor 50 40 80
    SetFontSize 45
    CustomAlertSound "speech/sapphire.mp3"
#fidelitas
Show
    BaseType "Topaz Ring"
    ItemLevel <= 16
    SetBorderColor 150 120 255
    SetBackgroundColor 50 40 80
    SetFontSize 45
    CustomAlertSound "speech/topaz.mp3"''')

    lines.append(highlighted_rare('Two-Stone Ring', 'speech/rare-two-stone.mp3'))
    lines.append(highlighted_rare('Sapphire Ring', 'speech/rare-sapphire.mp3'))
    lines.append(highlighted_rare('Topaz Ring', 'speech/rare-topaz.mp3'))
    lines.append(highlighted_rare('Ruby Ring', 'speech/rare-ruby.mp3'))
    lines.append(highlighted_rare('Coral Ring', 'speech/rare-coral.mp3'))
    lines.append(highlighted_rare('Iron Ring', 'speech/rare-iron.mp3'))
    lines.append(highlighted_rare('Jade Amulet', 'speech/rare-jade.mp3'))
    lines.append(highlighted_rare('Amber Amulet', 'speech/rare-amber.mp3'))
    lines.append(highlighted_rare('Lapis Amulet', 'speech/rare-lapis.mp3'))
    lines.append(highlighted_rare('Leather Belt', 'speech/rare-leather-belt.mp3'))
    lines.append(highlighted_rare('Heavy Belt', 'speech/rare-heavy-belt.mp3'))
    lines.append(highlighted_rare('Rustic Sash', 'speech/rare-rustic-sash.mp3'))

    # non-white boots
    lines.append('''Show
    Rarity = Rare
    Class Boots
    ItemLevel <= 35
    SetFontSize 45
    SetBackgroundColor 0 75 30 255
    SetBorderColor 150 150 150
    SetTextColor 255 255 119 255
    CustomAlertSound "speech/rare-boots.mp3"''')
    lines.append('''Show
    Rarity = Magic
    Class Boots
    ItemLevel <= 25
    SetFontSize 45
    SetBackgroundColor 0 0 0 255
    SetBorderColor 100 100 100 255
    SetTextColor 25 95 235 255
    CustomAlertSound "speech/magic-boots.mp3"''')

    lines.append(neversink_axe_section())
    # quest items
    lines.append('''Show
    Class "Quest" "Labyrinth"
    SetFontSize 45
    SetBorderColor 74 230 58
    PlayEffect Green
    MinimapIcon 1 Green Hexagon''')
    lines.append('''Show 
    Class "Map Fragments"
    SetFontSize 45
    SetTextColor 159 15 213 255
    SetBorderColor 159 15 213 255
    SetBackgroundColor 0 0 0 255
    PlayAlertSound 2 300
    PlayEffect White
    MinimapIcon 2 White Triangle''')

    lines.append('''Show
    BaseType "Quicksilver Flask"
    SetFontSize 45
    SetBorderColor 255 255 0 255
    SetBackgroundColor 25 25 0 255
    MinimapIcon 1 Yellow Hexagon
    PlayEffect Yellow
    CustomAlertSound "speech/quicksilver-flask.mp3"''')
    lines.append('''Show
    BaseType "Silver Flask"
    SetFontSize 45
    SetBorderColor 255 255 0 255
    SetBackgroundColor 25 25 0 255
    MinimapIcon 1 Yellow Hexagon
    PlayEffect Yellow
    CustomAlertSound "speech/onslaught-flask.mp3"''')
    lines.append('''Show
    BaseType "Granite Flask"
    SetFontSize 45
    SetBorderColor 255 255 0 255
    SetBackgroundColor 25 25 0 255
    MinimapIcon 1 Yellow Hexagon
    PlayEffect Yellow
    CustomAlertSound "speech/granite-flask.mp3"''')
    lines.append('''Show
    BaseType "Jade Flask"
    SetFontSize 45
    SetBorderColor 255 255 0 255
    SetBackgroundColor 25 25 0 255
    MinimapIcon 1 Yellow Hexagon
    PlayEffect Yellow
    CustomAlertSound "speech/jade-flask.mp3"''')
    lines.append('''Show
    BaseType "Stibnite Flask"
    SetFontSize 45
    SetBorderColor 255 255 0 255
    SetBackgroundColor 25 25 0 255
    MinimapIcon 1 Yellow Hexagon
    PlayEffect Yellow
    CustomAlertSound "speech/stibnite-flask.mp3"''')
    lines.append(neversink_flask_section())

    # chromatic item
    lines.append('''Show
    SocketGroup RGB
    SetFontSize 40
    SetBorderColor 0 255 0 255''')

    # early 3L with at least 2R or 1R1G
    lines.append('''Show
    SocketGroup RG RR
    LinkedSockets 3
    ItemLevel <= 16
    SetFontSize 40
    SetBorderColor 255 255 0 255
    SetBackgroundColor 100 0 0 255''')

    # 2x2(max) rares
    lines.append('''Show
    Rarity = Rare
    Height <= 2
    Width <= 2
    SetFontSize 40
    SetBackgroundColor 0 50 0 255
    SetBorderColor 150 150 150
    SetTextColor 255 190 0 255''')
    # 3x1(max) rares
    lines.append('''Show
    Rarity = Rare
    Height <= 3
    Width = 1
    SetFontSize 40
    SetBackgroundColor 0 50 0 255
    SetBorderColor 150 150 150
    SetTextColor 255 190 0 255''')
    # other rares
    lines.append('''Show
    Rarity = Rare
    SetFontSize 32
    SetBackgroundColor 0 0 0 219
    SetBorderColor 120 120 120
    SetTextColor 255 190 0 255''')
    # low ilvl magic items
    lines.append('''Show
    Rarity = Magic
    ItemLevel <= 20
    SetFontSize 32
    SetBackgroundColor 0 0 0 219
    SetBorderColor 120 120 120''')
    # other magic items
    lines.append('''Show
    Rarity = Magic
    SetFontSize 30
    SetBackgroundColor 0 0 0 219
    SetBorderColor 120 120 120''')
    # we don't want to hide jewellery
    lines.append('''Show
    Class "Rings" "Amulets"
    Rarity <= Magic
    SetFontSize 35''')
    # low ilvl normal items
    lines.append('''Show
    Rarity = Normal
    ItemLevel <= 10
    SetFontSize 30''')

    # failsafes:
    lines.append('''Hide
    Class "Amulets" "Belts" "Body Armour" "Boots" "Bows" "Claws" "Daggers" "Flask" "Gloves" "Helmets" "Jewel" "One Hand" "Quivers" "Rings" "Sceptre" "Shields" "Staves" "Two Hand" "Wand"
    SetFontSize 18
    SetBorderColor 0 0 0 150
    SetBackgroundColor 0 0 0 75''')
    lines.append('''Show
    SetFontSize 45
    SetTextColor 255 0 255 255
    SetBorderColor 255 0 255 255''')

    with open('xai.filter', 'w') as filter_file:
        filter_file.writelines(map('{0}\n'.format, lines))


def neversink_flask_section():
    return '''Show # %D5
    ItemLevel <= 5
    Class "Life Flasks"
    BaseType "Small"
    SetFontSize 45
    SetBorderColor 120 0 0
    SetBackgroundColor 0 0 0 255

Show # %D5
    ItemLevel <= 9
    ItemLevel >= 3
    Class "Life Flasks"
    BaseType "Medium"
    SetFontSize 45
    SetBorderColor 120 0 0
    SetBackgroundColor 0 0 0 255

Show # %D5
    ItemLevel <= 13
    ItemLevel >= 5
    Class "Life Flasks"
    BaseType "Large"
    SetFontSize 45
    SetBorderColor 120 0 0
    SetBackgroundColor 0 0 0 255
    CustomAlertSound "speech/large-life-flask.mp3"

Show # %D5
    ItemLevel <= 19
    ItemLevel >= 12
    Class "Life Flasks"
    BaseType "Greater"
    SetFontSize 45
    SetBorderColor 120 0 0
    SetBackgroundColor 0 0 0 255
    CustomAlertSound "speech/greater-life-flask.mp3"

Show # %D5
    ItemLevel <= 24
    ItemLevel >= 18
    Class "Life Flasks"
    BaseType "Grand"
    SetFontSize 45
    SetBorderColor 120 0 0
    SetBackgroundColor 0 0 0 255

Show # %D5
    ItemLevel <= 30
    ItemLevel >= 24
    Class "Life Flasks"
    BaseType "Giant"
    SetFontSize 45
    SetBorderColor 120 0 0
    SetBackgroundColor 0 0 0 255

Show # %D5
    ItemLevel <= 37
    ItemLevel >= 30
    Class "Life Flasks"
    BaseType "Colossal"
    SetFontSize 45
    SetBorderColor 120 0 0
    SetBackgroundColor 0 0 0 255

Show # %D5
    ItemLevel <= 42
    ItemLevel >= 36
    Class "Life Flasks"
    BaseType "Sacred"
    SetFontSize 45
    SetBorderColor 120 0 0
    SetBackgroundColor 0 0 0 255

Show # %D5
    ItemLevel <= 48
    ItemLevel >= 42
    Class "Life Flasks"
    BaseType "Hallowed"
    SetFontSize 45
    SetBorderColor 120 0 0
    SetBackgroundColor 0 0 0 255

Show # %D5
    ItemLevel <= 55
    ItemLevel >= 48
    Class "Life Flasks"
    BaseType "Sanctified"
    SetFontSize 45
    SetBorderColor 120 0 0
    SetBackgroundColor 0 0 0 255

Show # %D5
    ItemLevel <= 67
    ItemLevel >= 60
    Class "Life Flasks"
    BaseType "Divine"
    SetFontSize 45
    SetBorderColor 120 0 0
    SetBackgroundColor 0 0 0 255

Show # %D5
    ItemLevel <= 5
    Class "Mana Flasks"
    BaseType "Small"
    SetFontSize 45
    SetBorderColor 0 0 120
    SetBackgroundColor 0 0 0 255

Show # %D5
    ItemLevel <= 8
    ItemLevel >= 3
    Class "Mana Flasks"
    BaseType "Medium"
    SetFontSize 45
    SetBorderColor 0 0 120
    SetBackgroundColor 0 0 0 255

Show # %D5
    ItemLevel <= 12
    ItemLevel >= 5
    Class "Mana Flasks"
    BaseType "Large"
    SetFontSize 45
    SetBorderColor 0 0 120
    SetBackgroundColor 0 0 0 255
    CustomAlertSound "speech/large-mana-flask.mp3"

Show # %D5
    ItemLevel <= 18
    ItemLevel >= 12
    Class "Mana Flasks"
    BaseType "Greater"
    SetFontSize 45
    SetBorderColor 0 0 120
    SetBackgroundColor 0 0 0 255

Show # %D5
    ItemLevel <= 24
    ItemLevel >= 18
    Class "Mana Flasks"
    BaseType "Grand"
    SetFontSize 45
    SetBorderColor 0 0 120
    SetBackgroundColor 0 0 0 255

Show # %D5
    ItemLevel <= 30
    ItemLevel >= 24
    Class "Mana Flasks"
    BaseType "Giant"
    SetFontSize 45
    SetBorderColor 0 0 120
    SetBackgroundColor 0 0 0 255

Show # %D5
    ItemLevel <= 37
    ItemLevel >= 30
    Class "Mana Flasks"
    BaseType "Colossal"
    SetFontSize 45
    SetBorderColor 0 0 120
    SetBackgroundColor 0 0 0 255

Show # %D5
    ItemLevel <= 42
    ItemLevel >= 36
    Class "Mana Flasks"
    BaseType "Sacred"
    SetFontSize 45
    SetBorderColor 0 0 120
    SetBackgroundColor 0 0 0 255

Show # %D5
    ItemLevel <= 48
    ItemLevel >= 42
    Class "Mana Flasks"
    BaseType "Hallowed"
    SetFontSize 45
    SetBorderColor 0 0 120
    SetBackgroundColor 0 0 0 255

Show # %D5
    ItemLevel <= 55
    ItemLevel >= 48
    Class "Mana Flasks"
    BaseType "Sanctified"
    SetFontSize 45
    SetBorderColor 0 0 120
    SetBackgroundColor 0 0 0 255

Show # %D5
    ItemLevel <= 67
    ItemLevel >= 60
    Class "Mana Flasks"
    BaseType "Divine"
    SetFontSize 45
    SetBorderColor 0 0 120
    SetBackgroundColor 0 0 0 255

Show # %D5
    ItemLevel <= 70
    ItemLevel >= 65
    Class "Mana Flasks"
    BaseType "Eternal"
    SetFontSize 45
    SetBorderColor 0 0 120
    SetBackgroundColor 0 0 0 255'''


def neversink_axe_section():
    return '''# FilterBlade: Conditional Entry
Show 
    BaseType "Rusted Hatchet"
    Rarity <= Magic
    SetBorderColor 200 0 0
    SetFontSize 40
    ItemLevel <= 5

# FilterBlade: Conditional Entry
Show 
    BaseType "Jade Hatchet"
    Rarity <= Magic
    SetBorderColor 200 0 0
    SetFontSize 40
    ItemLevel <= 12

# FilterBlade: Conditional Entry
Show 
    BaseType "Boarding Axe"
    Rarity <= Magic
    SetBorderColor 200 0 0
    SetFontSize 40
    ItemLevel <= 17

# FilterBlade: Conditional Entry
Show 
    BaseType "Cleaver"
    Rarity <= Magic
    SetBorderColor 200 0 0
    SetFontSize 40
    ItemLevel <= 20

# FilterBlade: Conditional Entry
Show 
    BaseType "Broad Axe"
    Rarity <= Magic
    SetBorderColor 200 0 0
    SetFontSize 40
    ItemLevel <= 24

# FilterBlade: Conditional Entry
Show 
    BaseType "Arming Axe"
    Rarity <= Magic
    SetBorderColor 200 0 0
    SetFontSize 40
    ItemLevel <= 28

# FilterBlade: Conditional Entry
Show 
    BaseType "Decorative Axe"
    Rarity <= Magic
    SetBorderColor 200 0 0
    SetFontSize 40
    ItemLevel <= 32

# FilterBlade: Conditional Entry
Show 
    BaseType "Spectral Axe"
    Rarity <= Magic
    SetBorderColor 200 0 0
    SetFontSize 40
    ItemLevel <= 34

# FilterBlade: Conditional Entry
Show 
    BaseType "Etched Hatchet"
    Rarity <= Magic
    SetBorderColor 200 0 0
    SetFontSize 40
    ItemLevel <= 35

# FilterBlade: Conditional Entry
Show 
    BaseType "Jasper Axe"
    Rarity <= Magic
    SetBorderColor 200 0 0
    SetFontSize 40
    ItemLevel <= 38

# FilterBlade: Conditional Entry
Show 
    BaseType "Tomahawk"
    Rarity <= Magic
    SetBorderColor 200 0 0
    SetFontSize 40
    ItemLevel <= 41

# FilterBlade: Conditional Entry
Show 
    BaseType "Wrist Chopper"
    Rarity <= Magic
    SetBorderColor 200 0 0
    SetFontSize 40
    ItemLevel <= 44

# FilterBlade: Conditional Entry
Show 
    BaseType "War Axe"
    Rarity <= Magic
    SetBorderColor 200 0 0
    SetFontSize 40
    ItemLevel <= 47

# FilterBlade: Conditional Entry
Show 
    BaseType "Chest Splitter"
    Rarity <= Magic
    SetBorderColor 200 0 0
    SetFontSize 40
    ItemLevel <= 50

# FilterBlade: Conditional Entry
Show 
    BaseType "Ceremonial Axe"
    Rarity <= Magic
    SetBorderColor 200 0 0
    SetFontSize 40
    ItemLevel <= 53

# FilterBlade: Conditional Entry
Show 
    BaseType "Wraith Axe"
    Rarity <= Magic
    SetBorderColor 200 0 0
    SetFontSize 40
    ItemLevel <= 55

# FilterBlade: Conditional Entry
Show 
    BaseType "Engraved Hatchet"
    Rarity <= Magic
    SetBorderColor 200 0 0
    SetFontSize 40
    ItemLevel <= 56

# FilterBlade: Conditional Entry
Show 
    BaseType "Karui Axe"
    Rarity <= Magic
    SetBorderColor 200 0 0
    SetFontSize 40
    ItemLevel <= 58

# FilterBlade: Conditional Entry
Show 
    BaseType "Siege Axe"
    Rarity <= Magic
    SetBorderColor 200 0 0
    SetFontSize 40
    ItemLevel <= 60'''


if __name__ == '__main__':
    create_filter()
